const getItems = () => ({
  images: [],

  init() {
    this.getData()
  },

  getData() {
    const url = 'http://localhost:8000/api/v1/core/photo/'
    axios.get(url)
      .then(response => this.images = response.data)
  }
})
