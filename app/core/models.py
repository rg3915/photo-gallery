from django.db import models


class Photo(models.Model):
    title = models.CharField('título', max_length=100)
    description = models.TextField(null=True, blank=True)
    image = models.ImageField()

    class Meta:
        ordering = ('title',)
        verbose_name = 'foto'
        verbose_name_plural = 'fotos'

    def __str__(self):
        return f'{self.title}'
