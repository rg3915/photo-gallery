from http import HTTPStatus
from typing import List
from ninja import ModelSchema, Router
from ninja.orm import create_schema

from .models import Photo

router = Router()

PhotoSchema = create_schema(Photo)


@router.get('photo/', response=List[PhotoSchema], tags=['Photo'])
def list_photo(request):
    return Photo.objects.all()
