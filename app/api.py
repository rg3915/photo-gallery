from ninja import NinjaAPI

from app.core.api import router as core_router

api = NinjaAPI()

api.add_router('/core/', core_router)
