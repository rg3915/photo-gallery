# Photo Gallery

## Este projeto foi feito com:

* [Python 3.10.6](https://www.python.org/)
* [Django 4.2.6](https://www.djangoproject.com/)
* [Django-Ninja](https://django-ninja.rest-framework.com/)
* [AlpineJS](https://alpinejs.dev/)

## Como rodar o projeto?

* Clone esse repositório.
* Crie um virtualenv com Python 3.
* Ative o virtualenv.
* Instale as dependências.
* Rode as migrações.

```
git clone https://gitlab.com/rg3915/photo-gallery.git
cd photo-gallery

python -m venv .venv
source .venv/bin/activate

pip install -r requirements.txt

python contrib/env_gen.py

python manage.py migrate
python manage.py createsuperuser --username="admin" --email=""
```